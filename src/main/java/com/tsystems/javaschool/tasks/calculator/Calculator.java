package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        statement = statement.trim();
        Boolean s = Valid_Sym_Method(statement);
        ArrayList<ArrayList<Integer>> Array_P = new ArrayList<>();
        if(s){
            Array_P = Find_Parentheses(statement);
        }
        else{
            return null;
        }
        if(Array_P!=null){
            statement = Check_Positions(statement);
        }
        else{
            return null;
        }



        return statement;
    }
    public Boolean Valid_Sym_Method(String Input_Sym){
        String Valid_S = new String("1234567890.+-*/()");
        int length = Input_Sym.length();
        char[] Temp = new char[length];
        int j = 0;
        Input_Sym.getChars(0, length, Temp, 0);
        for(int i = 0; i < length; i++) {
            if(Valid_S.indexOf(Temp[i]) != -1){
                j++;
            }
            else {
                return false;
            }
        }
        return true;
    }

    public ArrayList<ArrayList<Integer>> Find_Parentheses(String Input_Sym){
        char LeftP = '(';
        char RightP = ')';
        ArrayList<Integer> LeftParentheses = new ArrayList<>();
        ArrayList<Integer> RightParentheses = new ArrayList<>();
        int length = Input_Sym.length();
        int i = 0;
        while(i <= length) {
            if (Input_Sym.charAt(i) == LeftP) {
                LeftParentheses.add(i);
                i++;
            }
            else if(Input_Sym.charAt(i) == RightP){
                RightParentheses.add(i);
                i++;
            }
            else{
                i++;
            }
        }
        int LeftParenthesesCount = LeftParentheses.size();
        int RightParenthesesCount = RightParentheses.size();
        if(LeftParenthesesCount == RightParenthesesCount){
            for(i = 0; i <= LeftParenthesesCount; i++){
                if(LeftParentheses.get(i) < RightParentheses.get(i)){
                    i++;
                }
                else{
                    return null;
                }
            }
        }
        else{
            return null;
        }
        ArrayList<ArrayList<Integer>> Array_P = new ArrayList<>();
        Array_P.add(LeftParentheses);
        Array_P.add(RightParentheses);
        return Array_P;
    }

    public String Check_Positions(String Input_Sym){
        String LeftP = new String("(");
        String RightP = new String(")");
        String Digits = new String("1234567890");
        String Dot = new String(".");
        String Signs = new String("+-*/");

        int i = 0;
        int lenght = Input_Sym.length();
        if(lenght <= 2){
            Input_Sym = Check_Unary_Operation(Input_Sym);
        }
        else{
            if((Input_Sym.substring(i,i).contains(LeftP)) | (Input_Sym.substring(i,i).contains(Digits))){

            }
        }


        return Input_Sym;
    }

    public String Check_Unary_Operation(String Input_Sym){


        return Input_Sym;
    }



    public TreeMap<Integer, String> Get_Tree_Vies(String Input_Sym){


        return new TreeMap<Integer, String>();
    }
    

}
